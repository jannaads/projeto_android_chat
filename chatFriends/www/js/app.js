var db = null;
var listaMsg = [];
var LIMITE_MENSAGEM = 20;
var usuario;
//var serviceUri = "http://10.3.188.207:8080/ServerMessagePDM/chat/service/";
//var serviceUri = "http://192.168.0.116:8080/ServerMessagePDM/chat/service/";
//var serviceUri = "http://192.168.0.103:8080/ServerMessagePDM/chat/service/";

var serviceUri = "http://192.168.0.13:8080/Server/chat/service/";
var idMsg;

angular.module('chatFriends', ['ionic','ionic.service.core', 'ngCordova'])

	.run(function ($ionicPlatform, $state, $cordovaSQLite, dao, usuarioService) {
		$ionicPlatform.ready(function () {
		
			if (window.cordova && window.cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			}
			if (window.StatusBar) {
				StatusBar.styleDefault();
			}		
			
			dao.createDB();
			usuarioService.logar();
		});
	})
	
	.config(function ($stateProvider, $urlRouterProvider) {
		$stateProvider.state('home', {
			url: '/home',
			templateUrl: 'tpl/form.html',
			controller: 'cadastroCTRL'
		})
		$stateProvider.state('chat', {
			url: '/chat',
			templateUrl: 'tpl/chat.html',
			controller: 'chatCTRL'
		})
		
		$stateProvider.state('initial', {
			url: '/initial',
			templateUrl: 'tpl/logo.html'
		})
		$urlRouterProvider.otherwise('/initial')
	})

	.controller('chatCTRL', ['$scope', 'mensagemService', 'cameraService', 'usuarioService', '$state', function ($scope, mensagemService, cameraService, 	usuarioService, $state) {
		$scope.mensagem = {};
		$scope.send = function(){
			mensagemService.enviar($scope.mensagem.conteudo, usuario.nome, device.uuid);
			$scope.mensagem.conteudo = "";
		};
		$scope.takePicture = function() {
			cameraService.capturar(usuario.nome, device.uuid);
			$scope.mensagem.conteudo = "";
		};
		$scope.compareToken = function(message) {
			if(message.token === device.uuid){
				return true;
			} else {
				return false;
			}
		};
		$scope.getLista = function(){
			return listaMsg;
		};
		$scope.compareTamanho = function(conteudo) {
			if(conteudo.length > 120){
				return true;
			} else {
				return false;
		}};
	}])
	
	.controller('cadastroCTRL', ['$scope', 'usuarioService', function ($scope, usuarioService) {
		$scope.usuario = {}
		$scope.cadastrar = function () {
			usuarioService.salvar($scope.usuario);
		};
	}])
	
	.factory('cameraService', function ($state, $cordovaCamera) {

		var options = { 
					quality : 75, 
					destinationType : Camera.DestinationType.DATA_URL, 
					sourceType : Camera.PictureSourceType.CAMERA, 
					allowEdit : true,
					encodingType: Camera.EncodingType.JPEG,
					targetWidth: 120,
					targetHeight: 160,
					popoverOptions: CameraPopoverOptions,
					saveToPhotoAlbum: false
				}
		
		return {
			capturar: function (nome, token) {
				$cordovaCamera.getPicture(options).then(function(imageData) {
					var imgURI = "data:image/jpeg;base64," + imageData;
										
					var xmlHttp = new XMLHttpRequest();
					xmlHttp.onreadystatechange = function() {					
						if (xmlHttp.readyState === 4) {
							if(xmlHttp.status === 200){
								//com sucesso
							}else{
								//erro
							}
						}
					};

					var msg = "{\"conteudo\":\""+imgURI+"\",\"nome\":\""+nome+"\",\"token\":\""+token+"\"}";
					xmlHttp.open("POST", serviceUri, true);
					xmlHttp.setRequestHeader("Content-type", "application/json");
					xmlHttp.send(msg);
				
				}, function(err) {
				// An error occured. Show a message to the user
					//console.log(err);
				});
			}
		}
	})
	
	.factory('usuarioService', function ($state, dao, mensagemService) {

		//comando sql
		var INSERIR_USUARIO = "INSERT INTO usuario(matricula, nome, cpf) VALUES (?,?,?)"
		var BUSCAR_USUARIO = "SELECT matricula, nome, cpf FROM usuario"

		return {
			salvar: function (s, callback) {
				dao.persiste(INSERIR_USUARIO, [s.matricula, s.nome, s.cpf], function (r) {
					if(r){
						dao.buscar(BUSCAR_USUARIO, function(res){
							usuario = res.rows.item(0);
						});
						/*
						* RECUPERA O ID PARA AS MENSAGENS
						*/
						mensagemService.ultimaMensagem(function(r){
							idMsg = r;
						});
						/*
						* ATIVANDO O TIMER PARA RECUPERAR AS MENSAGENS DO SERVIDOR
						*/
						var myTimer = setInterval(function(){
							mensagemService.mensagensChat(idMsg);							
						}, 2000);
						/*
						* REDIRECIONAMENTO
						*/
						$state.go('chat', {}, {reload: true })
					}else{
						$state.go('home', {}, {reload: true })
					}
				});				
			},
			logar: function (callback) {
				dao.buscar(BUSCAR_USUARIO, function(res){
					if(res.rows.length > 0){
						/*
						* RESTAURAR AS MENSAGENS SALVAS
						*/
						mensagemService.restaurar( function(l){
							listaMsg = l;
							/*
							* RECUPERA O ID DA ULTIMA MENSAGEM
							*/
							if(listaMsg.length > 0){
								idMsg = listaMsg[listaMsg.length-1].id;
							}else{
								mensagemService.ultimaMensagem(function(r){
									idMsg = r;
								});
							};
						});
						
						usuario = res.rows.item(0);
						/*
						* ATIVANDO O TIMER PARA RECUPERAR AS MENSAGENS DO SERVIDOR
						*/
						var myTimer = setInterval(function(){
							mensagemService.mensagensChat(idMsg);
						}, 2000);
						/*
						* REDIRECIONA PARA A PÁGINA DO CHAT
						*/
						$state.go('chat', {}, { reload: true })
					}else{
						$state.go('home', {}, { reload: true })
					}
				});
			}
		}
	})
	
	.factory('mensagemService', function ($state, dao) {

		//comando sql
		var INSERIR_MENSAGEM = "INSERT INTO mensagens(id, conteudo, nome, token) VALUES (?,?,?,?)";
		var LISTAR_MENSAGENS = "SELECT id, conteudo, nome, token FROM mensagens";
		var DELETAR_MENSAGEM = "DELETE FROM mensagens";
		var ULTIMO_ID_DA_MENSAGEM = "SELECT MAX(id) FROM mensagens";
		
		return {
			enviar: function (conteudo, nome, token) {
				
				var xmlHttp = new XMLHttpRequest();
				xmlHttp.onreadystatechange = function() {
					
					if (xmlHttp.readyState === 4) {
						if(xmlHttp.status > 199 && xmlHttp.status < 207){
							//com sucesso
							console.log("SUCESSO AO ENVIAR MSG PARA O SERVIDOR");
						}else{
							//erro
							console.log("ERRO AO ENVIAR MSG PARA O SERVIDOR");
						}
					}
					
				};

				var msg = "{\"conteudo\":\""+conteudo+"\",\"nome\":\""+nome+"\",\"token\":\""+token+"\"}";
				xmlHttp.open("POST", serviceUri, true);
				xmlHttp.setRequestHeader("Content-type", "application/json");
				xmlHttp.send(msg);
			},
			
			mensagensChat: function(id){
				var xmlHttp = new XMLHttpRequest();
				xmlHttp.onreadystatechange = function() {
						
					if (xmlHttp.readyState === 4) {
						console.log("STATUS: " + xmlHttp.status);
						if(xmlHttp.status > 199 && xmlHttp.status < 207){
							console.log("entrou no if");
							//com sucesso
							var textoResultado = xmlHttp.responseText;
							var resultado = JSON.parse(textoResultado);
							for (var i = 0; i < resultado.length; i++){
								listaMsg.push( resultado[i] )
							}
							if(listaMsg.length > 0){
								idMsg = listaMsg[listaMsg.length-1].id;
							}
							
							// SEMPRE ATUALIZANDO A LISTA DO BANCO LOCAL
							dao.executar(DELETAR_MENSAGEM, function(){
									var index;
									if( listaMsg.length <= LIMITE_MENSAGEM){
										index = 0;
									}else{
										index = listaMsg.length - LIMITE_MENSAGEM;
									}
									for( var i = index; i < listaMsg.length; i++){
										dao.persiste(INSERIR_MENSAGEM, [listaMsg[i].id, listaMsg[i].conteudo, listaMsg[i].nome, listaMsg[i].token], function (r) {
										});
									}				
																		
							});
						}else{
							// caso dê erro na conexão
								dao.buscar(LISTAR_MENSAGENS, function(res){
										var result = [];
										for (var i = 0; i < res.rows.length; i++){
											result.push(res.rows.item(i))
										}
										listaMsg = result;
									});
						}
					}
				};
				
				xmlHttp.open("GET", serviceUri + id, true);
				xmlHttp.setRequestHeader("Content-type", "application/json");
				xmlHttp.send();
			},

			ultimaMensagem: function(callback){
				
				var xmlHttp = new XMLHttpRequest();
				xmlHttp.onreadystatechange = function() {
						
					if (xmlHttp.readyState === 4) {
						if(xmlHttp.status > 199 && xmlHttp.status < 207){
							//com sucesso
							var textoResultado = xmlHttp.responseText;
							var resultado = JSON.parse(textoResultado);
							callback(resultado);
							//
						}else{
							//erro
						}
					}
				};
				xmlHttp.open("GET", serviceUri + "last", true);
				xmlHttp.setRequestHeader("Content-type", "application/json");
				xmlHttp.send();
			},
			
			restaurar: function (callback) {
				dao.buscar(LISTAR_MENSAGENS, function(res){
					var result = [];
					for (var i = 0; i < res.rows.length; i++){
						result.push(res.rows.item(i))
					}
					callback(result);
				});
			},
			
			ultimoId: function(callback){
				dao.buscar(ULTIMO_ID_DA_MENSAGEM, function(res){
					callback(res);
				});
			}
		}
	})
	
	.factory('dao', function ($cordovaSQLite) {
		
		return {
			persiste: function (sql, parametros, callback) {
				$cordovaSQLite.execute(db, sql, parametros).then(function (res) {
					callback(true)
				}, function (err) {
					console.error(err);
					callback(false);
				})
			},
			buscar: function(sql, callback){
				$cordovaSQLite.execute(db, sql).then(function (res) {
					callback(res)
				}, function (err) {
					console.error(err);
					callback([]);
				})
			},
			executar: function(sql, callback){
				$cordovaSQLite.execute(db, sql, []).then(function (res) {
					callback();
				}, function (err) {
					console.error(false);
				});
			},
			createDB: function(callback){
				db = window.sqlitePlugin.openDatabase({name: "chat.db", androidDatabaseImplementation: 2, androidLockWorkaround: 1});

				$cordovaSQLite.execute(db,
					"CREATE TABLE IF NOT EXISTS usuario (matricula integer primary key, nome text, cpf text)"
				);
				
				$cordovaSQLite.execute(db,
					"CREATE TABLE IF NOT EXISTS mensagens (id integer primary key, conteudo blob, nome text, token text)"
				);
				
			}
		}
	})
	
